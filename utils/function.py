import math
import numpy as np
from utils.toolbox import *
from scipy.sparse import csgraph
random_seed = 42

def MMR(xO, xO_keys, xN, xN_keys, G):
    '''
    Mutual Mapping Relationships:
    G <--
        [G: OxO      psiNO: OxN
         psiON: NxO  psiNN: NxN]
    '''
    if list(xN): # only calculate MMR if new features appear
        # psiNN = np.dot(np.linalg.pinv(xN),xN)
        psiNN = np.zeros([len(xN_keys), len(xN_keys)])
        MatToNestedDict(psiNN,xN_keys,xN_keys,G)
        if list(xO): # calculate N->O and O->N MMR if old features exist
            # psiON = np.dot(np.linalg.pinv(xO), xN)
            psiON = np.zeros([len(xN_keys), len(xO_keys)])
            MatToNestedDict(psiON,xO_keys,xN_keys,G)
            # psiNO = np.dot(np.linalg.pinv(xN),xO)
            psiNO = np.zeros([len(xO_keys), len(xN_keys)])
            MatToNestedDict(psiNO,xN_keys,xO_keys,G)


def Pred(x, x_keys, x_rec_keys, x_in_U_keys, W, w_old, w_new, G, p, y, haveSup=1):
    '''
    :param x: Arriving instance
    :param x_keys: Keys of the existing features
    :param x_rec_keys: Keys of the reconstructed features
    :param x_in_U_keys: Keys of all features in U_t
    :param W: Weights of all features in U_t
    :param w_old: Weights of the features of x
    :param w_new: Weights of the reconstructed features
    :param G: Mapping matrix
    :param p: Self-tuning parameter
    :param y: Ground label
    '''
    y_hat_1 = np.dot(x, w_old)
    x_in_U = (1/ len(list(x))) * np.dot(x,G)
    y_bar = np.dot(x_in_U, W)
    x_in_U_dict = {k:v for k,v in zip(x_in_U_keys, x_in_U.flatten())}
    '''Rec Error'''
    x_ori = DictToList(x_in_U_dict, x_keys, WorX='X') # Take the observable entries
    x_diff = x - x_ori
    RecError = np.linalg.norm(x_diff) ** 2
    if not np.isscalar(RecError): RecError = np.asscalar(RecError)
    if list(x_rec_keys):
        x_rec = DictToList(x_in_U_dict, x_rec_keys, WorX='X') # Take the reconstructed entries
        y_hat_2 = np.dot(x_rec,w_new)
    else: y_hat_2 = 0 # Only exe. at the 1st iteration
    error = 0
    # Prevent Grad. explosion
    if y*y_hat_1 > 1:
        y_diff_1 = 0
    else:
        y_diff_1 = y - y_hat_1
    if y*y_hat_2 > 1:
        y_diff_2 = 0
    else:
        y_diff_2 = y - y_hat_2
    SL_1 = np.abs(y_diff_1) ** 2
    SL_2 = np.abs(y_diff_2) ** 2
    y_hat = p * y_hat_1 + (1-p) * y_hat_2 # weighted combination
    # if SL_1 <= SL_2:
    #     y_hat = y_hat_1
    # else:
    #     y_hat = y_hat_2
    if np.sign(y_hat) != y: error = 1
    '''Sup Loss'''
    y_diff = y - y_hat
    y_bar_diff = y - y_bar
    SupLoss = np.abs(y_diff) ** 2
    if not np.isscalar(SupLoss): SupLoss = np.asscalar(SupLoss)
    if not np.isscalar(SL_1): SL_1 = np.asscalar(SL_1)
    if not np.isscalar(SL_2): SL_2 = np.asscalar(SL_2)
    return error, x_diff, RecError, y_bar_diff, SupLoss, SL_1, SL_2



def RecError_Grad(x, x_diff, x_keys, W_keys):
    '''
    :param: x_diff: Discrepancy between
    the arriving instance and the reconstructed one
    :param: x_keys: Keys of the features of x
    :param: W_keys: Keys of the feature in U_t
    :return: gradient on G
    '''
    In_Mat = IndicatorMatrix(x_keys, W_keys)
    temp1 = np.dot(x.T, x_diff)
    deltaG = (-2/len(list(x))) * np.dot(temp1, In_Mat)
    return deltaG

def SupLoss_Grad(x, y_diff, x_keys, W_keys, W, G):
    '''
    :param: y_diff: Supervised loss suffered by
    only depending on the reconstructed x
    :return: gradient on G and W
    '''
    deltaW = (-2/len(list(x))) * y_diff * np.dot(G.T, x.T)
    deltaG = (-2/len(list(x))) * y_diff * np.dot(x.T, W.T)
    return deltaG, deltaW

def Graph_Reg(W, G, beta, x_keys, W_keys):
    '''
    :param W: Full weight vector
    :param G: Square Mapping Matrix (|U_t| X |U_t|)
    :param x_,W_keys: Casting G to G_(t)
    '''
    W_l1 = L1_reg_grad(W)
    G_laplacian = csgraph.laplacian(G, normed=False)
    # print('G:', G_laplacian)
    In_Mat = IndicatorMatrix(x_keys, W_keys)
    deltaW = beta * W_l1 + (1-beta) * np.dot((G_laplacian + G_laplacian.T), W)
    deltaG = (1-beta) * (np.dot(W.T, W) - np.dot(W, W.T))
    deltaG = np.dot(In_Mat, deltaG)
    return deltaG, deltaW


def L1_reg_grad(W):
    # print('W:', W)
    W1 = (W >= 0) * 1
    W2 = (W < 0) * -1
    # print('L1_W: ', (W1 + W2))
    return (W1 + W2)

def suffer_loss(y, X, W, Lambda, grad=False):
    '''Rennie and Srebro's smoothed version,
     make it differentiable at y.y_hat =1'''
    lossCap = y * np.dot(X, W)[0]
    if grad == False:
        if lossCap >= 1:
            return 0 + Lambda * np.linalg.norm(W, ord=1)
            # return 0
        elif lossCap <= 0:
            return 0.5-lossCap + Lambda * np.linalg.norm(W, ord=1)
        else:
            return 0.5 * (1-lossCap)**2 + Lambda * np.linalg.norm(W, ord=1)
    else:
        '''return arrays, not scalar'''
        if lossCap >= 1:
            return 0 + Lambda * L1_reg_grad(W)
            # return 0
        elif lossCap <= 0:
            return -1 * y * X.T + Lambda * L1_reg_grad(W)
        else:
            return -1 * y * X.T * (1-lossCap) + Lambda * L1_reg_grad(W)

def suffer_loss_M(y, X, W, M, grad=False):
    '''Prevent the gradient explosion'''
    lossCap = y * np.dot(np.dot(X, M), W)
    if grad == False:
        if lossCap >= 1:
            return 0
        elif lossCap <= 0:
            return 0.5-lossCap
        else:
            return 0.5*(1-lossCap)**2
    elif grad == 'M':
        if lossCap >= 1:
            return np.zeros(M.shape)
        elif lossCap <= 0:
            return -1 * y * np.dot(X.T, W.T)
        else:
            return -1 * y * (1-lossCap) * np.dot(X.T, W.T)
    else:
        if lossCap >= 1:
            return np.zeros(W.shape)
        elif lossCap <= 0:
            return -1 * y * np.dot(X, M).T
        else:
            return -1 * y * (1-lossCap) * np.dot(X, M).T

def update_psi(p1, p2, loss1, loss2, eta):
    # loss1 = math.fabs(loss1) / (math.fabs(loss1)+math.fabs(loss2))
    # loss2 = 1-loss1
    exp1 = np.exp(-1*loss1 * eta)
    exp2 = np.exp(-1*loss2 * eta)
    p1 = p1 * exp1 / (p1 * exp1 + p2 * exp2)
    p2 = 1 - p1
    return p1, p2

def SGD(y, W, x, CostFunction, eta, psi, beta_1):
    y_hat = np.dot(x, W)[0]
    delta_W = suffer_loss(y, x, W, Lambda=beta_1, grad=True)
    sufferLoss = suffer_loss(y, x, W, Lambda=beta_1)
    # W = W - psi*eta*delta_W
    W = -1 * psi*eta*delta_W
    return y_hat, W, sufferLoss

def SGA_M(y, W_full, x_partial, M, CostFunction, eta, lambdaReg=1, psi=1):
    x_full = np.dot(x_partial, M)
    y_hat = np.dot(x_full, W_full)[0]
    sufferLoss = suffer_loss_M(y, x_partial, W_full, M)
    M_grad = suffer_loss_M(y, x_partial, W_full, M, grad='M')
    M_updated = M - psi * eta * M_grad
    W_grad = suffer_loss_M(y, x_partial, W_full, M, grad='W')
    W_updated = -1 * psi * eta * W_grad
    return y_hat, M_updated, W_updated, sufferLoss

