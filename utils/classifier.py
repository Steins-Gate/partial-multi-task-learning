import numpy as np
import pandas as pd
import math
import utils.function as F
import utils.toolbox as T
import LoadData as P

class OCDS:
    def __init__(self, args):
        '''
            Data is stored as list of dictionaries.
            Label is stored as list of scalars.
        '''

        data = P.DataSet(args.DataName)
        self.data = data.data
        self.label = data.label
        self.W = {}
        self.M = {}
        self.psi_1 = 0.5
        self.psi_2 = 0.5
        self.error = 0
        self.accuracy = 0
        self.i = 0
        self.variant = 'None'

        self.AccLoss_1 = 0
        self.AccLoss_2 = 0

        ''' Hyper-parameters '''
        self.alpha = float(args.alpha)
        self.beta_1 = float(args.beta1)
        self.beta_2 = float(args.beta2)
        self.exam_acc = 0

    def check(self):
        if len(self.data) == len(self.label):
            print("Load data finish, # of Neg. and Pos. samples are ...")
        unique_elements, counts_elements = np.unique(self.label, return_counts=True)
        print( np.asarray((unique_elements, counts_elements)) )


    def eta(self, t):
        ''' Learning Rate: self-adaptive'''
        return self.beta_2 * math.ceil(t**(-0.5))
        # return 0.01

    def train(self):
        '''
            When data stream arrives, it will be divided into two parts:
        '''
        print("Learning Start... ")
        for round in range(10):
            for i, row in enumerate(self.data):
                if round == 0:
                    self.i = i
                    y = self.label[i][0] # groundtruth label
                    x = T.DictToList(row, WorX='X') # current instance, 1xN
                    x_keys = list(row.keys()) # corresponding feature # of current instance
                    x_keys.sort()
                    W_keys = list(self.W.keys())
                    W_keys.sort()
                    eta = self.eta(i+1)

                    ''' 1: partial features that have been met with the classifier '''
                    shared_W_keys = list(set(x_keys) & set(W_keys))
                    shared_W_keys.sort()
                    shared_x = T.DictToList(row, shared_W_keys, WorX='X')
                    ''' 2: partial features that are new  '''
                    extra_x_keys = list(set(x_keys) - set(W_keys))
                    extra_x_keys.sort()
                    extra_x = T.DictToList(row, extra_x_keys, WorX='X')

                    ''' No shared W found -> all features in X are not trained -> init as zeros '''
                    if not shared_W_keys:
                        shared_W_keys = x_keys.copy()
                        shared_W = np.zeros((len(shared_W_keys), 1))
                        W_init = {k:v for k,v in zip(shared_W_keys, shared_W.flatten())}
                        self.W.update(W_init)
                        M_init = np.dot(np.linalg.pinv(x), x) # Mapping the current feature space to itself
                        T.MatToNestedDict(M_init, x_keys, shared_W_keys, self.M)

                    ''' If new features appeal -> init corresponding W as zeros '''
                    if extra_x_keys:
                        extra_W = np.zeros((len(extra_x_keys), 1))
                        W_init_extra = {k:v for k,v in zip(extra_x_keys, extra_W.flatten())}
                        self.W.update(W_init_extra)
                        M_init = np.dot(np.linalg.pinv(extra_x), x) # Mapping the extra feature space to full space
                        T.MatToNestedDict(M_init, extra_x_keys, x_keys, self.M)
                        ''' To complete M, map the shared W to the extra feature space '''
                        if shared_W_keys:
                            M_SharedToExtra = np.dot(np.linalg.pinv(shared_x), extra_x)
                            T.MatToNestedDict(M_SharedToExtra, shared_W_keys, extra_x_keys, self.M)

                ''' Case 1: update the shared W with OGD '''
                W = T.DictToList(self.W, x_keys)
                y_hat_1, delta_W_1, loss_1 = F.SGD(y, W, x, 'L2', eta, self.psi_1, beta_1=self.beta_1)
                if not np.isscalar(loss_1):
                    loss_1 = np.asscalar(loss_1)
                # print('Instance No.%d | Loss: %.4f' %(i, loss_1))
                # print('Learning Rate: ', eta)

                ''' Case 2: map the current instance to the full space;
                            update M and full-space W according to the loss '''
                W_full = T.DictToList(self.W)
                W_keys = list(self.W.keys())
                W_keys.sort()
                M = T.FindInDict(x_keys, W_keys, self.M)
                # print(self.M)
                y_hat_2, M_updated, delta_W_full, loss_2 = F.SGA_M(y, W_full, x, M, 'L2', eta, psi=self.psi_2)
                if not np.isscalar(loss_2):
                    loss_2 = np.asscalar(loss_2)
                T.MatToNestedDict(M_updated, x_keys, W_keys, self.M)
                y_hat = self.psi_1 * y_hat_1 + self.psi_2 * y_hat_2

                if self.variant == 'None':
                    if loss_1 < loss_2:
                        y_hat = y_hat_1
                    elif loss_1 > loss_2:
                        y_hat = y_hat_2
                    else:
                        y_hat = self.psi_1 * y_hat_1 + self.psi_2 * y_hat_2
                elif self.variant == 'obs':
                    y_hat = y_hat_1
                else: y_hat = y_hat_2

                loss_total = self.psi_1 * loss_1 + self.psi_2 * loss_2
                self.AccLoss_1 += loss_1
                self.AccLoss_2 += loss_2
                loss_total = loss_total/math.ceil(math.sqrt(i+1))

                loss_1 = self.AccLoss_1 / (self.AccLoss_1 + self.AccLoss_2)
                loss_2 = 1-loss_1

                self.psi_1, self.psi_2 = F.update_psi(self.psi_1, self.psi_2, loss_1, loss_2, self.alpha)

                ''' 
                    Update the W in the current feature space:
                    1. Normal GD only consider the current W, no problem here;
                    2. How to take advantage of full space information here?
                       Current -- M --> Full, so
                       Full -- M_inverse --> Current
                '''
                W_hyper_updated_2 = np.dot(delta_W_full.T, np.linalg.pinv(M))
                W_UPDATE = W + delta_W_1 + W_hyper_updated_2.T
                W_UPDATE_dict = {k:v for k,v in zip(shared_W_keys, W_UPDATE.flatten())}
                self.W.update(W_UPDATE_dict)

                if np.sign(y_hat) != y: self.error += 1
                # if i % 50 == 0 and i >= 0:
                #     print('%dth iter.| Loss %.6f | Error Rate %.4f ' %(self.i, loss_total, self.error/(i+1)))
            # print('Error: ', self.error)
            self.exam_acc = 1 - (self.error / len(self.label))
            print("Epoch: %d | Training Accuracy: %.4f" % (round, self.exam_acc))
            if self.exam_acc > .98: break
            else: self.error=0

