# Generative Learning with Partial Multi-Tasks

## Data
Five pre-processed datasets in `.pickle` format are provided for evaluation.
To simulate partial examples, 50% data examples are randomly removed for each task.
The other used datasets can be downloaded from [UCI Machine Learning Repository](https://archive.ics.uci.edu/ml/datasets.php).

## Getting started
Libraries used:  
* Numpy  
* Scipy  
* Sklearn  
* Pandas  
* Pickle  
* Argparse  

For Window users:
```
python main.py -DataName CAL500 -lambda_1 0.6 -lambda_2 0.9 
```
`-Dataname`: options include `CAL500`, `emotions`, `slashdot`, `delicious`, and `CCYS`.  
`-lambda_1`: parameter to absorb the different scale between the reconstructed error and supervised loss.   
`-lambda_2`: parameter to control the influence of dictionary encoding error.  
