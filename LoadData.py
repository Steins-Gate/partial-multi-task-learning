import pandas as pd
import numpy as np
import random
import os
from sklearn.utils import shuffle
import math
from sklearn.preprocessing import LabelEncoder, OneHotEncoder
from sklearn.preprocessing import normalize, scale
import pickle

dataset_path = os.path.abspath(os.path.dirname(__file__))
dataset_path = os.path.join(dataset_path, 'Datasets/')

def DataToDict(data, startKey=0):
    '''To enlarge the dataset as the setting in NIPS 2017 paper'''
    dictRow = []
    for (i, row) in enumerate(data):
        mydict = {v+startKey: k for v, k in enumerate(row)}
        dictRow.append(mydict)
    return dictRow

def MakeCapricious(data, sparsity = 1, seed=42):
    '''The data is stored as a LIST of dictionaries (any ops on np array works)'''
    random.seed(seed)
    maxFeatureNum = len(data[0]) # number of features in first chunk
    featureKept = math.ceil(len(data[0]) * sparsity)
    chunkSize = math.ceil(len(data) * sparsity)
    howSparseLength = featureKept
    for (i, vec) in enumerate(data):
        if (i+1) % chunkSize == 0:
            howSparseLength = min(howSparseLength + featureKept, maxFeatureNum)
        # print(howSparseLength)
        rDelSamples = random.sample(range(maxFeatureNum), howSparseLength)
        # print(rDelSamples)
        for k, v in vec.copy().items():
            if k not in rDelSamples:
                del vec[k]
    return data

def count_value(nparr):
    '''Summary the array as counting the occurrence of different values'''
    unique_elements, counts_elements = np.unique(nparr, return_counts=True)
    return np.asarray((unique_elements, counts_elements))

class DataSet:
    def __init__(self, file):
        self.data = pickle.load(open(str(dataset_path + file + ".dat"), "rb"))
        self.label = pd.read_csv(dataset_path + file + ".y", header=None, engine='python').as_matrix()

class LoadData:
    '''
        1. svmguide3:
            21 features, 1284 instances
        2. HAPT:
            561 features, 10929 instances
        3. splice (One-Hot normlized):
            60 features (acutally 480), 3190 instances
        4. german:
            20 features, 1000 instances
        5. spambase:
            57 features, 4601 instances
        6. magic04:
            10 features, 19020 instances
        7. a8a:
            123 features, 32560 instances
        8. wbc:
            9 features, 699 instances
        9. wdbc:
            30 features, 569 instances
        10. wpbc:
            33 features, 198 instances
        11. ionosphere:
            34 features, 351 instances
        12. australian:
            14 features, 690 instances
        13. credit_a:
            15 features, 690 instances
        14. diabetes:
            8 features, 768 instances
        15. dna:
            180 features, 949 instances
        16. kr_vs_kp:
            36 features, 3196 instances
        17. EN-FR (Reuter):
            21531 -> 24893 features, 18758 instances
        18. EN-GR (Reuter):
            21531 -> 34215 features, 18758 instances
        19. EN-IT (Reuter):
            21531 -> 15506 features, 18758 instances
        20. EN-SP (Reuter):
            21531 -> 11547 features, 18758 instances
    '''
    def __init__(self, file, norm=None, seed=42):
        print('LOAD '+str(file)+' ...')
        data = pd.read_csv(dataset_path + file + "_X.csv", header=None, engine='python').as_matrix()
        label = pd.read_csv(dataset_path + file + "_y.csv", header=None, engine='python').as_matrix()
        self.data, self.label = shuffle(data, label, random_state=seed)
        self.nFeature = data.shape[1]
        self.nRow = data.shape[0]
        self.seed = seed
    def mercury(self, retain=0.5):
        dataDict = DataToDict(self.data)
        self.data = MakeCapricious(dataDict, retain, self.seed)
        self.data, self.label = shuffle(self.data, self.label, random_state=self.seed)
        savePickle(self.data, "CCYS.dat", self.label, "CCYS.y")

def savePickle(data, dataPath, label, labelPath):
    pickle.dump(data, open(dataPath, 'wb'))
    pd.DataFrame(label).to_csv(str(labelPath), index=False, header=None)





