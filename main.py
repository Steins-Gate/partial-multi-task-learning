import numpy as np
import pandas as pd
from utils.classifier import *
import LoadData as P
import ModelVariants
import argparse
import time



def main():

    parser = argparse.ArgumentParser(description="Options")

    parser.add_argument('-DataName', action='store', dest='DataName', default='CAL500')
    parser.add_argument('-lambda_1', action='store', dest='alpha', default=0.05)
    parser.add_argument('-lambda_2', action='store', dest='beta1', default=0.07)
    parser.add_argument('-beta2', action='store', dest='beta2', default=0.005)

    args = parser.parse_args()

    learner = OCDS(args)
    # learner.check()
    print("Finish Loading Data ...")
    start_time = time.time()
    learner.train()
    end_time = time.time()
    print("Running Time: ", (end_time - start_time))






if __name__ == '__main__':
    main()






