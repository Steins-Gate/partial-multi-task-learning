import numpy as np
import pandas as pd
import math
from utils.function import *
from utils.toolbox import *

class ModelVariants:
    def __init__(self, data, label, alpha = 1, beta = 0.001):

        self.data = data # a list of dictionaries
        self.label = label # scalars
        self.W = {}
        self.M = {}
        self.error = 0
        self.accuracy = 0
        self.i = 0
        self.cummulative_1 = 0
        self.cummulative_2 = 0
        self.tempCuLoss_1 = 0
        self.tempCuLoss_2 = 0

        ''' Tradeoff parameters '''
        self.alpha = alpha # tradeoff to learning the Rec Error
        self.beta = beta # tradeoff to graph regularizer
        self.p = 0.5

    def check(self):
        if len(self.data) == len(self.label):
            print("Load data finish, # of Neg. and Pos. samples are ...")

    def p_self_turning(self):
        exp1 = self.p * np.exp(-1 * self.cummulative_1 * self.eta())
        exp2 = (1 - self.p) * np.exp(-1 * self.cummulative_2 * self.eta())
        p_new = exp1 / (exp1 + exp2)
        self.p = p_new

    def eta(self): # learning rate in updating ensemble coefficient P
        ''' P: self-adaptive'''
        # return 0.001 * math.ceil(math.sqrt(1/self.i))
        return 0.001

    def tau(self):
        ''' # learning rate in updatiing G and W '''
        return 0.01 * math.ceil(math.sqrt(1/self.i))
        # return 0.001

    def train(self):
        '''
        instances in numpy row array,
        weights in numpy column array.
        '''

        print("Start... ")
        for i,row in enumerate(self.data):
            self.i = i+1
            y = self.label[i][0] # groundtruth label
            x = DictToList(row, WorX='X') # current instance, 1xN
            x_keys = list(row.keys()) # corresponding feature # of current instance
            x_keys.sort()
            W_keys = list(self.W.keys())
            W_keys.sort()

            ''' 1: Features that having by instance and classifier '''
            shared_W_keys = list(set(x_keys) & set(W_keys))
            shared_W_keys.sort()
            shared_x = DictToList(row, shared_W_keys, WorX='X')
            ''' 2: New features '''
            extra_x_keys = list(set(x_keys) - set(W_keys))
            extra_x_keys.sort()
            extra_x = DictToList(row, extra_x_keys, WorX='X')
            ''' 3: Unobservable features'''
            extra_W_keys = list(set(W_keys) - set(x_keys))
            extra_W_keys.sort()

            ''' Initialization for W and G '''
            if not shared_W_keys: # Exe. only at the 1st iteration when W is empty
                print('Initialzie W ...')
                shared_W_keys = x_keys.copy()
                shared_W = np.zeros((len(shared_W_keys), 1))
                W_init = {k:v for k,v in zip(shared_W_keys, shared_W.flatten())}
                self.W.update(W_init)
                M_init = np.dot(np.linalg.pinv(x), x) # Mapping the current feature space to itself
                MatToNestedDict(M_init, x_keys, shared_W_keys, self.M)

            elif extra_x_keys: # Exe. once new features appear
                print('Pad G ...')
                extra_W = np.zeros((len(extra_x_keys), 1))
                W_init_extra = {k:v for k,v in zip(extra_x_keys, extra_W.flatten())}
                self.W.update(W_init_extra)
                MMR(x,x_keys,extra_x,extra_x_keys,self.M)


            extra_W = DictToList(self.W, extra_W_keys)
            shared_W = DictToList(self.W, x_keys)
            W_keys = list(self.W.keys())
            W = DictToList(self.W, W_keys)
            M = FindInDict(x_keys, W_keys, self.M)
            M_U_t = FindInDict(W_keys, W_keys, self.M)


            ''' Making prediction, suffering loss'''
            error, x_diff, RecError, y_diff, SupLoss, SL1, SL2 = Pred(x, x_keys, extra_W_keys,
                                                                        W_keys, W, shared_W, extra_W,
                                                                        M, self.p, y)
            self.error += error

            ''' Gradient on Rec Error'''
            deltaG_H = RecError_Grad(x, x_diff, x_keys, W_keys)

            ''' Gradient on Sup Loss '''
            deltaG_L, delta_W = SupLoss_Grad(x, y_diff, x_keys, W_keys, W, M)

            ''' Gradient on Graph Reg '''
            deltaG_R, delta_W_R = Graph_Reg(W, M_U_t, self.beta, x_keys, W_keys)

            ''' Updating W and G '''
            W = W - self.tau() * (delta_W + self.beta * L1_reg_grad(W))
            M = M - self.tau() * (deltaG_L + self.alpha * deltaG_H)

            W_UPDATE_dict = {k: v for k, v in zip(W_keys, W.flatten())}
            self.W.update(W_UPDATE_dict)

            MatToNestedDict(M, x_keys, W_keys, self.M)

            ''' Updating Rule for p '''

            # SL1 = SL1 / (SL1 + SL2)
            # SL2 = 1 - SL1

            # self.cummulative_1 += self.p * SL1
            # self.cummulative_2 += (1-self.p) * SL2

            # self.cummulative_1 = self.eta() * SL1
            # self.cummulative_2 = self.eta() * SL2

            self.tempCuLoss_1 += SL1
            self.tempCuLoss_2 += SL2

            self.cummulative_1 = self.tempCuLoss_1 / (self.tempCuLoss_1 + self.tempCuLoss_2)
            self.cummulative_2 = 1 - self.cummulative_1

            self.p_self_turning()


            # print('Label: %d, Prediction: %.4f' %(y, y_hat))
            # if np.sign(y_hat) != y: self.error += 1
            if i % 10 == 0 and i >= 0:
                print('%dth iter.| Sup Loss %.6f | Error Rate %.4f ' % (self.i, SupLoss, self.error/(i+1)))
        # print('Error: ', self.error)
        print("Accuracy: ", 1 - (self.error / len(self.label)))




if __name__=="__main__":
    if [1]:
        print ('have 1')
